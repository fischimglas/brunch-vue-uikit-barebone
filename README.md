# Brunch Vue Barebones

@lastUpdate: 14/05/2020

A _barebones_ Brunch skeleton for [Vue.js](https://vuejs.org/) - minimal dependencies!

## Installation

1. Install Brunch globally

```bash
npm install -g brunch
```

2. Install Brunch globally

```bash
yarn install
```
